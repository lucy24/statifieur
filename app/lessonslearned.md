# Learning Android: notes

## Where to start

I'm not *new* to Java, it's only that i've used it last at school 8 years ago, back in 2005. I've been into C++ since then, and more recently into Python. I dived into Java again not by nostalgia, only as a means to create my own Android applications. I just have bought a new smartphone, my first one, a Wiko Cink Peax (yep), and i intended to program things.

It seems logical, but i started [here](http://developer.android.com/training/index.html).

Then for more specific questions, there's the [API Guide](http://developer.android.com/guide/components/index.html). I find the documentation really useful, you get the basic use cases with code samples, so overall you may have most of your job done with copy-paste.

## how to use a third party library

I wanted to plot charts, more specifically simple histograms. Tried to find a library, found [AChartEngine](http://code.google.com/p/achartengine/)

Only my Java times were 7 years back, and i had not a clue how to link against that library. SO to the rescue :-).
[Adding a library jar to an Eclipse Android project](http://stackoverflow.com/questions/3642928/adding-a-library-jar-to-an-eclipse-android-project)

some help on [making the bar chart look good](http://stackoverflow.com/questions/11309065/my-android-achartengine-is-already-working-but-how-to-make-it-look-good)

## listviews

It crashed until i read a tutorial about it to do it right...
http://www.ace-art.fr/wordpress/2010/07/21/tutoriel-android-partie-6-les-listview/
and also: http://stackoverflow.com/questions/3718523/create-listview-programmatically

## Menus
Adding a menu icon was actually pretty easy. The most difficult would be to draw a pretty icon.
http://developer.android.com/guide/topics/ui/menus.html

## dialogs

There are two kinds of DialogFragment, i don't know which is best, but you don't get both the same way so you may have strange errors.
http://stackoverflow.com/questions/13175713/i-am-getting-an-error-the-method-showfragmentmanager-string
http://stackoverflow.com/questions/6596628/problems-with-alertdialog-show-in-fragment-android

Apparently the one from the support package is better. Only why?

## logs
http://developer.android.com/reference/android/util/Log.html

## edit text
[Stop EditText from gaining focus at Activity startup](http://stackoverflow.com/questions/1555109/stop-edittext-from-gaining-focus-at-activity-startup)

## ADT
One morning, on opening ADT software, I had a message telling me I had to update (OK), and when checking for updates, no updates found. And the projects wouldn't compile anymore. StackOverflow to the rescue :-) : [When I load ADT why do I receive the error �The Android SDK requires Android Developer Toolkit version XX.X.X or above?�](http://stackoverflow.com/questions/15105730/when-i-load-adt-why-do-i-receive-the-error-the-android-sdk-requires-android-dev)

## SQLite

I started with an ugly trick to save all the data in the shared preference. Compared to saving to an sqlite database, that was very easy and lightweight. But that was obviously limited.

## Overall opinion

Getting back to Java after bites of Python is hardly a pleasure.

## Liens utiles

* http://www.androidviews.net/ : des custom views, des libs, �
* http://androiddevweekly.com/ : une mailing list de qualit� (avec des tuto, articles, libs, bouquins, �)
* http://androidweekly.net/ (idem)
* http://androidkickstartr.com/ g�n�rateur d�appli de base (pour bien d�marrer)
* http://androidannotations.org/ framework pour r�duire la taille du code