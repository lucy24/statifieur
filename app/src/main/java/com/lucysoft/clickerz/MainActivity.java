package com.lucysoft.clickerz;

import java.util.List;

import com.lucysoft.clickerz.R;
import com.lucysoft.clickerz.StatEditorFragment.OnStatValidatedListener;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.util.Pair;
import android.util.TypedValue;
import android.view.Menu;
import android.view.View;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.ArrayAdapter;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

public class MainActivity extends Activity implements OnStatValidatedListener{
	private static final String TAG = "com.lucysoft.clickerz.MainActivity";
	
	public final static String EXTRA_TITLE = "com.lucysoft.clickerz.TITLE";
	public final static String EXTRA_CHOICE1 = "com.lucysoft.clickerz.CHOICE1";
	public final static String EXTRA_CHOICE2 = "com.lucysoft.clickerz.CHOICE2";
	public final static String EXTRA_STAT_ID = "com.lucysoft.clickerz.STAT_ID";

	private ListView mListView = null;
	private ClickStorage mStatsDatabase = null;
	
	private long[] mMapPosToStatId = null;
	
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        
		Log.v(TAG, "Create Storage");
		mStatsDatabase = new ClickStorage(getApplicationContext());

        TextView textView2 = (TextView) findViewById(R.id.textView2);
        textView2.setTextSize(TypedValue.COMPLEX_UNIT_PX, 
        		getResources().getDimension(R.dimen.fragment_title_fontsize));
		
        mListView = (ListView)findViewById(R.id.listView1);
        	 
        mListView.setOnItemClickListener(new OnItemClickListener() {
            public void onItemClick(AdapterView<?> parent, View view,
                    int position, long id) {
            				String selectedTitle = ((TextView) view).getText().toString();
            				long selectedStatId = mMapPosToStatId[position];
                 			Toast.makeText(getApplicationContext(),
                 					"You selected : "+selectedTitle, Toast.LENGTH_SHORT).show();
                 			sendStatMessage(view, selectedStatId);
            		}
        });
        
    }
    
    @Override
    protected void onStart() {
    	super.onStart();  // Always call the superclass method first

    	String[] titlesList = {};

    	assert(mStatsDatabase != null); 
    	List<Pair<String, Long>> titles_list = mStatsDatabase.get_stats();
    	int nbStats = titles_list.size();
    	if(nbStats>0) {
        	titlesList = new String[nbStats];
        	mMapPosToStatId = new long[nbStats];
        	
        	for(int pos=0; pos<nbStats; ++pos) {
        		Pair<String, Long> stat_pair = titles_list.get(pos);
        		titlesList[pos] = stat_pair.first;
        		mMapPosToStatId[pos] = stat_pair.second;
        	}
    		
    	}

    	StatEditorFragment statFragment = (StatEditorFragment) getFragmentManager().findFragmentById(R.id.stat_fragment_id_main);
    	statFragment.ClearTextInput();
    	mListView.setAdapter(new ArrayAdapter<String>(this, android.R.layout.simple_list_item_1, titlesList));

    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.main, menu);
        return true;
    }
    
    private String getTextFromViewById(int id) {
    	EditText editText = (EditText) findViewById(id);
    	String text = editText.getText().toString();
    	return text;
    }
    
    
    /** Called when the user clicks the Send button */
    public void onStatValidated() {
        // Do something in response to button
    	Intent intent = new Intent(this, StatCounterActivity.class);
    	intent.putExtra(EXTRA_TITLE, getTextFromViewById(R.id.edit_title));
    	intent.putExtra(EXTRA_CHOICE1, getTextFromViewById(R.id.edit_choice1));
    	intent.putExtra(EXTRA_CHOICE2, getTextFromViewById(R.id.edit_choice2));
		Log.v(TAG, "Start StatCounterActivity from sendMessage");
        startActivity(intent);
    }
    
    /** Called when the user clicks the Send button */
    public void sendStatMessage(View view, long stat_id) {
        // Do something in response to button
    	Intent intent = new Intent(this, StatCounterActivity.class);
    	intent.putExtra(EXTRA_STAT_ID, stat_id);
		Log.v(TAG, "Start StatCounterActivity from sendStatMessage");
        startActivity(intent);
    }
    
}
