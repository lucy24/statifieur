package com.lucysoft.clickerz;

import android.provider.BaseColumns;

/*
 * Define stats database structure
 */
public final class StatsDatabaseContract {
	public StatsDatabaseContract() {}
	
	public static final class StatStruct implements BaseColumns {

		// This class cannot be instantiated
		private StatStruct() {}

		/**
		 * The table names offered by this provider
		 */
		public static final String STATS_TABLE_NAME = "stats";

		/*
		 * Column definitions
		 */

		/**
		 * Column name for the title of the stat
		 * <P>Type: TEXT</P>
		 */
		public static final String STAT_COLUMN_NAME_TITLE = "stat_title";

		/**
		 * Column name for the creation timestamp
		 * <P>Type: INTEGER (long from System.currentTimeMillis())</P>
		 */
		public static final String STAT_COLUMN_NAME_CREATE_DATE = "created";

		/**
		 * Column name for the modification timestamp
		 * <P>Type: INTEGER (long from System.currentTimeMillis())</P>
		 */
		public static final String STAT_COLUMN_NAME_MODIFICATION_DATE = "modified";

		/**
		 * The default sort order for this table
		 */
		public static final String STAT_DEFAULT_SORT_ORDER = STAT_COLUMN_NAME_MODIFICATION_DATE + " DESC";

	}


	public static final class CountersStruct implements BaseColumns {

		// This class cannot be instantiated
		private CountersStruct() {}

		/**
		 * The table names offered by this provider
		 */
		public static final String COUNTERS_TABLE_NAME = "counters";

		/*
		 * Column definitions
		 */

		/**
		 * Column names of the stat counter
		 * <P>Type: TEXT</P>
		 */
		public static final String COUNTERS_COLUMN_NAME_TITLE = "counter_title";
		public static final String COUNTERS_COLUMN_NAME_STAT_FK = "stat_id";


		/**
		 * The default sort order for this table
		 */
		public static final String COUNTERS_DEFAULT_SORT_ORDER = _ID + " ASC";

	}


	public static final class ClickStruct implements BaseColumns {

		// This class cannot be instantiated
		private ClickStruct() {}

		/**
		 * The table names offered by this provider
		 */
		public static final String CLICKS_TABLE_NAME = "clicks";

		/*
		 * Column definitions
		 */

		/**
		 * Column names for clicks
		 * <P>Type: TEXT</P>
		 */
		public static final String CLICKS_COLUMN_NAME_COUNTER_FK = "counter_id";
		public static final String CLICKS_COLUMN_NAME_DATE = "date";


		/**
		 * The default sort order for this table
		 */
		public static final String CLICKS_DEFAULT_SORT_ORDER = CLICKS_COLUMN_NAME_DATE + " ASC";

	}

	
}
