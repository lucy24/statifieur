package com.lucysoft.clickerz;

import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import android.content.ContentValues;
import android.database.Cursor;
import android.util.Log;

public class StatsData {
	private static final String TAG = "com.lucysoft.clickerz.StatsData";

	private long mStatId;
	private String mStatTitle;
	private long mCreationDate;
	private long mModifiedDate;
	private boolean mIsModified = false; 
	private List<String> mCountersTitle = new ArrayList<String>();
	private long[] mCounterIds = null;
	private HashMap<String, Long> mCountersTitleToIdMap = new HashMap<String,Long>();
	private HashMap<String, Integer> mCountersTitleToPosMap = new HashMap<String,Integer>();
	
	private int mNbCounters;
	private int[] mClickCounts = null;
	private double[] mClickDistribution = null;
	
	private DecimalFormat percentFormatter = new DecimalFormat("#");
	
	private class Click {
		private long mId;
		private long mCounterId;
		private long mDate;
		
		Click(long counter_id) {
			mId = -1;
			mCounterId = counter_id;
			mDate = System.currentTimeMillis();
		}
		
		
		boolean Write(ClickStorage data) throws Exception {
			if(mId<1) {
				ContentValues values = new ContentValues();
				values.put(StatsDatabaseContract.ClickStruct.CLICKS_COLUMN_NAME_COUNTER_FK, mCounterId);
				values.put(StatsDatabaseContract.ClickStruct.CLICKS_COLUMN_NAME_DATE, mDate);
				mId = data.insertValues(StatsDatabaseContract.ClickStruct.CLICKS_TABLE_NAME, values);
				if(mId<1)
					throw new Exception("Error couldn't save click.");
				return true;
			}
			return false;
		}
		

	}
	
	private List<Click> mClickList = null;
	
	StatsData(String stat_title, List<String> counters_title, ClickStorage data) {
		mStatId = -1;
		mStatTitle = stat_title;
		mCreationDate = System.currentTimeMillis();
		mModifiedDate = mCreationDate;
		mIsModified = true;
		
		// counters properties
		mCountersTitle = counters_title;
		mNbCounters = counters_title.size();
		mCounterIds = new long[mNbCounters];
		
		// initialize counts, default java array is filled with zeros
		mClickCounts = new int[mNbCounters];
		mClickDistribution = new double[mNbCounters];
		
		for(int k=0; k<mNbCounters; ++k) {
			mCountersTitleToPosMap.put(mCountersTitle.get(k), k);
		}
		
		mClickList = new ArrayList<Click>();

		Log.v(TAG, "Now save the new stat!");
		Write(data);
	}
	
	StatsData(long stat_id, ClickStorage data) {
		mStatId = stat_id;
		mStatTitle = null;
		Read(data);
	}
	
	private List<Click> ReadOneCounterClicks(ClickStorage data, long counter_id) {
		List<Click> clicks = new ArrayList<Click>();
		if(counter_id<1) {
			return clicks;
		}

		// SELECT
		String[] columns = {
				StatsDatabaseContract.ClickStruct._ID,
				StatsDatabaseContract.ClickStruct.CLICKS_COLUMN_NAME_DATE,
				StatsDatabaseContract.ClickStruct.CLICKS_COLUMN_NAME_COUNTER_FK
		};
		
		// WHERE
		String selection = StatsDatabaseContract.ClickStruct.CLICKS_COLUMN_NAME_COUNTER_FK 
				+ "=?";
		
		// ? binds to 
		String[] selectionArgs = {String.valueOf(counter_id)};
		
		// FROM
		String tables = StatsDatabaseContract.ClickStruct.CLICKS_TABLE_NAME;
		
		Log.v(TAG, "run the query...");
		Cursor cursor = data.query(selection, selectionArgs, columns, tables, null);
		Log.v(TAG, "[OK]");
		if (cursor == null) {
			Log.v(TAG, "but empty :-(");
			return clicks;
		} else {
			int nbClicks = cursor.getCount();
			Log.v(TAG, "yes! nb clicks =" + nbClicks);
			cursor.moveToFirst();
			
			do {
				int cursorPos = cursor.getPosition();
				Log.v(TAG, "cursor at " + cursorPos);
				int iIndex = cursor.getColumnIndexOrThrow(StatsDatabaseContract.ClickStruct._ID);
				int dIndex = cursor.getColumnIndexOrThrow(StatsDatabaseContract.ClickStruct.CLICKS_COLUMN_NAME_DATE);
				int cIndex = cursor.getColumnIndexOrThrow(StatsDatabaseContract.ClickStruct.CLICKS_COLUMN_NAME_COUNTER_FK);
							
				Click newClick = new Click(cursor.getLong(cIndex));
				newClick.mId = cursor.getLong(iIndex);
				newClick.mDate = cursor.getLong(dIndex);
				clicks.add(newClick);
				
			} while(cursor.moveToNext());
			
		}
		return clicks;
	}
	
	boolean ReadStats(ClickStorage data) {
		// SELECT
		String[] columns = {
				StatsDatabaseContract.StatStruct._ID,
				StatsDatabaseContract.StatStruct.STAT_COLUMN_NAME_TITLE,
				StatsDatabaseContract.StatStruct.STAT_COLUMN_NAME_CREATE_DATE
		};

		// WHERE
		String selection = StatsDatabaseContract.StatStruct._ID 
				+ "=?";

		// ? binds to 
		String[] selectionArgs = {String.valueOf(mStatId)};

		// FROM
		String tables = StatsDatabaseContract.StatStruct.STATS_TABLE_NAME;

		Log.v(TAG, "run the query...");
		Cursor cursor = data.query(selection, selectionArgs, columns, tables, null);
		Log.v(TAG, "[OK]");
		if (cursor == null || !cursor.moveToFirst()) {
			Log.v(TAG, "but empty :-(");
			return false;
		} else {
			mNbCounters = cursor.getCount();
			Log.v(TAG, "cursor at" + cursor.getPosition());
			int tIndex = cursor.getColumnIndexOrThrow(StatsDatabaseContract.StatStruct.STAT_COLUMN_NAME_TITLE);
			int cIndex = cursor.getColumnIndexOrThrow(StatsDatabaseContract.StatStruct.STAT_COLUMN_NAME_CREATE_DATE);
			Log.v(TAG, "index=" + tIndex);
			String name=cursor.getString(tIndex);
			Log.v(TAG, "name=" + name);
			mStatTitle = name;
			mCreationDate = cursor.getLong(cIndex);
		}
		return mStatId>0;
	}
	
	boolean ReadCounters(ClickStorage data) {
		if(mStatId<1)
			ReadStats(data);

		// SELECT
		String[] columns = {
				StatsDatabaseContract.CountersStruct._ID,
				StatsDatabaseContract.CountersStruct.COUNTERS_COLUMN_NAME_TITLE,
				StatsDatabaseContract.CountersStruct.COUNTERS_COLUMN_NAME_STAT_FK
		};
		
		// WHERE
		String selection = StatsDatabaseContract.CountersStruct.COUNTERS_COLUMN_NAME_STAT_FK 
				+ "=?";
		
		// ? binds to 
		String[] selectionArgs = {String.valueOf(mStatId)};
		
		// FROM
		String tables = StatsDatabaseContract.CountersStruct.COUNTERS_TABLE_NAME;
		
		Log.v(TAG, "run the query...");
		Cursor cursor = data.query(selection, selectionArgs, columns, tables, null);
		Log.v(TAG, "[OK]");
		if (cursor == null) {
			Log.v(TAG, "but empty :-(");
			return false;
		} else {
			mNbCounters = cursor.getCount();
			if(mNbCounters<2)
			{
				Log.v(TAG, "yek? nb counters =" + mNbCounters);
				return false;
			}
			Log.v(TAG, "yes! nb counters =" + mNbCounters);
			mCounterIds = new long[mNbCounters];
			cursor.moveToFirst();
			
			do {
				int cursorPos = cursor.getPosition();
				Log.v(TAG, "cursor at " + cursorPos);
				int cIndex = cursor.getColumnIndexOrThrow(StatsDatabaseContract.CountersStruct._ID);
				int nIndex = cursor.getColumnIndexOrThrow(StatsDatabaseContract.CountersStruct.COUNTERS_COLUMN_NAME_TITLE);
				Log.v(TAG, "index=" + cursor.getLong(cIndex));
				mCounterIds[cursorPos] = cursor.getLong(cIndex);
				
				String name = cursor.getString(nIndex);
				mCountersTitle.add(name);
				mCountersTitleToIdMap.put(name, mCounterIds[cursorPos]);
				mCountersTitleToPosMap.put(name, cursorPos);
								
				Log.v(TAG, "name=" + name);
			} while(cursor.moveToNext());
			
		}
		
		// initialize counts, default java array is filled with zeros
		mClickCounts = new int[mNbCounters];
		mClickDistribution = new double[mNbCounters];
		return true;
	}
	
	private boolean ReadClicks(ClickStorage data) {
		mClickList = new ArrayList<Click>();
		for(int c=0; c<mNbCounters; ++c) {
			List<Click> listC = ReadOneCounterClicks(data, mCounterIds[c]);
			mClickCounts[c] = listC.size();
			mClickList.addAll(listC);
		}
		return !mClickList.isEmpty();
	}
	
	boolean Read(ClickStorage data) {
		boolean isOk = ReadStats(data);
		isOk = isOk && ReadCounters(data);
		isOk = isOk && ReadClicks(data);
		return isOk;
	}

	private void WriteStat(ClickStorage data) throws Exception {
		Log.v(TAG, "WriteStat " + mStatTitle);
		if(mStatId<1) {
			ContentValues values = new ContentValues();
			values.put(StatsDatabaseContract.StatStruct.STAT_COLUMN_NAME_TITLE, mStatTitle);
			values.put(StatsDatabaseContract.StatStruct.STAT_COLUMN_NAME_CREATE_DATE, mCreationDate);
			values.put(StatsDatabaseContract.StatStruct.STAT_COLUMN_NAME_MODIFICATION_DATE, mModifiedDate);
			mStatId = data.insertValues(StatsDatabaseContract.StatStruct.STATS_TABLE_NAME, values);
			if(mStatId<1)
				throw new Exception("Error couldn't save new stat.");
		}
		else {
			ContentValues values = new ContentValues();
			values.put(StatsDatabaseContract.StatStruct.STAT_COLUMN_NAME_MODIFICATION_DATE, mModifiedDate);
			String where = StatsDatabaseContract.StatStruct._ID + "=?";
			String[] whereArgs={String.valueOf(mStatId)};
			long modId = data.updateValues(StatsDatabaseContract.StatStruct.STATS_TABLE_NAME, values, where, whereArgs);
			assert(modId == 1);
		}
		Log.v(TAG, "WriteStat [OK] to " + mStatId);
	}
	
	private void WriteCounters(ClickStorage data) throws Exception {
		Log.v(TAG, "WriteCounters");
		if(mStatId<1) {
			throw new Exception("Error: Cannot write counters without valid stat id, given:" + mStatId);
		}
		for(int k=0; k<mNbCounters; ++k) {
			if(mCounterIds[k]<1) {
				ContentValues values = new ContentValues();
				values.put(StatsDatabaseContract.CountersStruct.COUNTERS_COLUMN_NAME_STAT_FK, mStatId);
				values.put(StatsDatabaseContract.CountersStruct.COUNTERS_COLUMN_NAME_TITLE, mCountersTitle.get(k));
				mCounterIds[k] = data.insertValues(StatsDatabaseContract.CountersStruct.COUNTERS_TABLE_NAME, values);
				if(mCounterIds[k]<1) {
					throw new Exception("Error: Failed to insert counter values for " + mCountersTitle.get(k));
				}
				mCountersTitleToIdMap.put(mCountersTitle.get(k), mCounterIds[k]);
				mCountersTitleToPosMap.put(mCountersTitle.get(k), k);
			}
		}
		Log.v(TAG, "WriteCounters [OK]");
	}
	
	private void WriteClicks(ClickStorage data) throws Exception {
		Log.v(TAG, "WriteClicks");
		int nbClicksSaved = 0;
		for (Click temp : mClickList) {
			if(temp.Write(data))
				nbClicksSaved++;
		}
		if(nbClicksSaved>0)
			Log.v(TAG, "new clicks: " + nbClicksSaved);
		Log.v(TAG, "WriteClicks [OK]");
	}
	
	void Write(ClickStorage data) {
		if(!mIsModified)
			return;

		data.StartTransaction();
		try {
			WriteStat(data);
			WriteCounters(data);
			WriteClicks(data);
			data.Commit();
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} finally {
			data.EndTransaction();
		}

	}
	
	private int DeleteStat(ClickStorage data) throws Exception {
		Log.v(TAG, "DeleteStat");
		int mStatsDeleted = 0;

		String table_name = StatsDatabaseContract.StatStruct.STATS_TABLE_NAME;
		// Define 'where' part of query.
		String selection = StatsDatabaseContract.StatStruct._ID + "=?";
		// Specify arguments in placeholder order.
		String[] selectionArgs = { String.valueOf(mStatId) };
		mStatsDeleted += data.deleteValues(table_name, selection, selectionArgs);
		if(mStatsDeleted>0)
			Log.v(TAG, "stats deleted: " + mStatsDeleted);
		Log.v(TAG, "DeleteStat [OK]");
		return mStatsDeleted;
	}
	
	private int DeleteCounters(ClickStorage data) throws Exception {
		Log.v(TAG, "DeleteCounters");
		int mCountersDeleted = 0;

		String table_name = StatsDatabaseContract.CountersStruct.COUNTERS_TABLE_NAME;
		// Define 'where' part of query.
		String selection = StatsDatabaseContract.CountersStruct.COUNTERS_COLUMN_NAME_STAT_FK + "=?";
		// Specify arguments in placeholder order.
		String[] selectionArgs = { String.valueOf(mStatId) };
		mCountersDeleted += data.deleteValues(table_name, selection, selectionArgs);
		if(mCountersDeleted>0)
			Log.v(TAG, "counters deleted: " + mCountersDeleted);
		Log.v(TAG, "DeleteCounters [OK]");
		return mCountersDeleted;
	}
	
	private int DeleteClicks(ClickStorage data) throws Exception {
		Log.v(TAG, "DeleteClicks");
		int nbClicksDeleted = 0;
		for(int k=0; k<mNbCounters; ++k) {
			if(mCounterIds[k]>=1) {
				String table_name = StatsDatabaseContract.ClickStruct.CLICKS_TABLE_NAME;
				// Define 'where' part of query.
				String selection = StatsDatabaseContract.ClickStruct.CLICKS_COLUMN_NAME_COUNTER_FK + "=?";
				// Specify arguments in placeholder order.
				String[] selectionArgs = { String.valueOf(mCounterIds[k]) };
				nbClicksDeleted += data.deleteValues(table_name, selection, selectionArgs);
			}
		}
		if(nbClicksDeleted>0)
			Log.v(TAG, "clicks deleted: " + nbClicksDeleted);
		Log.v(TAG, "DeleteClicks [OK]");
		return nbClicksDeleted;
	}

	int Delete(ClickStorage data) {
		data.StartTransaction();
		int nbRowsDeleted = 0;
		try {
			nbRowsDeleted += DeleteClicks(data);
			nbRowsDeleted += DeleteCounters(data);
			nbRowsDeleted += DeleteStat(data);
			Log.v(TAG, "rows deleted: " + nbRowsDeleted);
			data.Commit();
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} finally {
			data.EndTransaction();
		}
		return nbRowsDeleted;
	}

	private void incrementCounter(String counter_name) {
		if(!mCountersTitleToIdMap.containsKey(counter_name)) {
			Log.e(TAG, "Error! unknown counter name" + counter_name);
		}
		mClickList.add(new Click(mCountersTitleToIdMap.get(counter_name)));
		mClickCounts[mCountersTitleToPosMap.get(counter_name)] ++;
		mModifiedDate = System.currentTimeMillis();
		mIsModified = true;
	}

	void onClickCounter(int counter_pos) {
		String counter_name = mCountersTitle.get(counter_pos);
		if ((counter_pos != mCountersTitleToPosMap.get(counter_name))) throw new AssertionError();
		incrementCounter(counter_name);
	}
	/**
	 * Compute statistics: distribution
	 */
	void updatePercents() {

		double sumClicks = 0;
		for(int k=0; k<mNbCounters; ++k)
		{
			sumClicks += mClickCounts[k];
		}
		if(sumClicks>0)
		{
			for(int k=0; k<mNbCounters; ++k)
			{
				mClickDistribution[k] = mClickCounts[k]/sumClicks * 100;
			}
		}
	}

	void Reset() {
		// Currently: delete current session's clicks
		// TODO: invalidate all previous clicks in database
		for(int k=0; k<mNbCounters; ++k) {
			mClickCounts[k] = 0;
			mClickDistribution[k] = 0;
			mClickList.clear();
		}
		mModifiedDate = System.currentTimeMillis();
	}
	
	
	///// ACCESSORS
	String getButtonText(int buttonIdx) {
		assert(buttonIdx < mNbCounters);
		return String.format("%d", 
				mClickCounts[buttonIdx]);
	}

	String getPercentText(int buttonIdx) {
		assert(buttonIdx < mNbCounters);
		return String.format("%s%%", 
				percentFormatter.format(mClickDistribution[buttonIdx]));
	}

	int GetNbCounters() {
		return mNbCounters;
	}

	double[] GetDistribution() {
		return mClickDistribution;
	}

	String getStatTitle() {
		return mStatTitle;
	}

	String[] GetCounterNames() {
		String[] names = {};
		names = mCountersTitle.toArray(names);
		return names;
	}

	boolean IsInDb() {
		return mStatId>0;
	}
	
}
