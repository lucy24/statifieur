package com.lucysoft.clickerz;

import android.app.Activity;
import android.app.Fragment;
import android.os.Bundle;

import android.util.TypedValue;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.EditorInfo;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.TextView.OnEditorActionListener;
import com.lucysoft.clickerz.R;

public class StatEditorFragment extends Fragment {
	 // Container Activity must implement this interface
    public interface OnStatValidatedListener {
        public void onStatValidated();
    }
    
    OnStatValidatedListener mListener;
    
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_edit_stat, container, false);

        TextView textView1 = (TextView) view.findViewById(R.id.textView1);
        textView1.setTextSize(TypedValue.COMPLEX_UNIT_PX,
        		getResources().getDimension(R.dimen.fragment_title_fontsize));
        
        EditText editText = (EditText) view.findViewById(R.id.edit_choice2);
        editText.setOnEditorActionListener(new OnEditorActionListener() {
            @Override
            public boolean onEditorAction(TextView v, int actionId, KeyEvent event) {
                boolean handled = false;
                if (actionId == EditorInfo.IME_ACTION_SEND) {
                	mListener.onStatValidated();
                    handled = true;
                }
                return handled;
            }
        });
        return view;
    }
    
    
    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);
        try {
            mListener = (OnStatValidatedListener) activity;
        } catch (ClassCastException e) {
            throw new ClassCastException(activity.toString() + " must implement OnStatValidatedListener");
        }
    }


    public void ClearTextInput() {
    	int text_input_ids[] = {R.id.edit_title, R.id.edit_choice1, R.id.edit_choice2 };
    	for(int id : text_input_ids) {
        	EditText editText = (EditText) getView().findViewById(id);
        	editText.getEditableText().clear();
        	editText.clearFocus();
    	}
    }
    
}