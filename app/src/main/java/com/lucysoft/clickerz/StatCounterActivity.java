package com.lucysoft.clickerz;

import java.util.ArrayList;
import java.util.List;


import android.annotation.SuppressLint;
import android.annotation.TargetApi;
import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.os.Build;
import android.os.Bundle;
import android.os.Vibrator;
import android.support.v4.app.DialogFragment;
import android.support.v4.app.FragmentActivity;
import android.support.v4.app.NavUtils;
import android.util.Log;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.TextView;

import com.github.mikephil.charting.charts.BarChart;
import com.github.mikephil.charting.components.Legend;
import com.github.mikephil.charting.components.XAxis;
import com.github.mikephil.charting.components.YAxis;
import com.github.mikephil.charting.data.BarData;
import com.github.mikephil.charting.data.BarDataSet;
import com.github.mikephil.charting.data.BarEntry;

import com.github.mikephil.charting.formatter.LargeValueFormatter;
import com.lucysoft.clickerz.R;



public class StatCounterActivity extends FragmentActivity
implements NoticeDialogFragment.NoticeDialogListener{
	private static final String TAG = "com.lucysoft.clickerz.StatCounterActivity";

	private StatsData mStatsData = null;
	private ClickStorage mStatsDatabase = null;

	private boolean mIsStatDeleted;

	/** The chart view that displays the data. */
	private BarChart mChartView;
	
	private Vibrator mVibrator;
	
	private static final long LIGHT_VIBRATION = 30;
	private static final long NORMAL_VIBRATION = 50;
	//private static final long STRONG_VIBRATION = 80;
	private long[] mVibrationPatterns;
	


	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		MenuInflater inflater = getMenuInflater();
		inflater.inflate(R.menu.stat_counter, menu);
		return true;
	}

	@SuppressLint("NewApi")
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);

		mIsStatDeleted = false;
		Log.v(TAG, "OnCreate StatCounterActivity");

		Log.v(TAG, "SetContentView StatCounterActivity");
		setContentView(R.layout.activity_stat_counter);
		// Show the Up button in the action bar.
		setupActionBar();
		

		mVibrator = (Vibrator) getSystemService(Context.VIBRATOR_SERVICE);
		mVibrationPatterns = new long[2];
		mVibrationPatterns[0] = LIGHT_VIBRATION;
		mVibrationPatterns[1] = NORMAL_VIBRATION;

		Log.v(TAG, "Create Storage");
		mStatsDatabase = new ClickStorage(getApplicationContext());
		mStatsDatabase.get_stats();


		if (mChartView == null) {
			mChartView = findViewById(R.id.chart);
		}

		SetChartStyle();
		Log.v(TAG, "Created !!!!");
	}


	@Override
	public void onResume() {
		Log.v(TAG, "umonRese StatCounterActivity");
		super.onResume(); 
		
		String[] labels = mStatsData.GetCounterNames();

		Log.v(TAG, "Create the text views");
		// Create the text view
		TextView textView = (TextView) findViewById(R.id.titleView);
		textView.setTextSize(40);
		textView.setText(mStatsData.getStatTitle());

		TextView textView1 = (TextView) findViewById(R.id.textView1);
		textView1.setText(labels[0]);
		TextView textView2 = (TextView) findViewById(R.id.textView2);
		textView2.setText(labels[1]);

		mStatsData.updatePercents();
		showButtonsClicks();

		UpdateChartValues();
	}
	
	@Override
	public void onStart() {
		Log.v(TAG, "onStart StatCounterActivity");
		super.onStart(); 
		Intent intent = getIntent();
		long statId = intent.getLongExtra(MainActivity.EXTRA_STAT_ID, -1);
		String title = intent.getStringExtra(MainActivity.EXTRA_TITLE);
		String mChoice1 = intent.getStringExtra(MainActivity.EXTRA_CHOICE1);
		String mChoice2 = intent.getStringExtra(MainActivity.EXTRA_CHOICE2);

		retrieveLastStatifier(statId);
		if(mStatsData == null || !mStatsData.IsInDb()) {
			assert(mChoice1 != null && mChoice2 != null);
			List<String> countersTitle = new ArrayList<String>();
			countersTitle.add(mChoice1);
			countersTitle.add(mChoice2);
			mStatsData = new StatsData(title, countersTitle, mStatsDatabase);
		}
	}
	private void persistCurrentStatifier() {
		// don't save something deleted
		if(mIsStatDeleted)
			return;

		mStatsData.Write(mStatsDatabase);
	}

	private void retrieveLastStatifier(long stat_id) {
		if(mStatsData == null)
			mStatsData = new StatsData(stat_id, mStatsDatabase);
		else
			mStatsData.Read(mStatsDatabase);
	}

	private void deleteCurrentStats() {
		mStatsData.Delete(mStatsDatabase);
		mIsStatDeleted = true;
	}

	private void SetChartStyle() {
		mChartView.setDrawGridBackground(false);

		XAxis xAxis = mChartView.getXAxis();
		xAxis.setDrawAxisLine(false);
		xAxis.setDrawGridLines(false);
		xAxis.setDrawLabels(false);

		mChartView.getXAxis().setAxisMinimum(0.f);
		mChartView.getXAxis().setAxisMaximum(1.f);

		YAxis leftAxis = mChartView.getAxisLeft();
		//leftAxis.setTypeface(tfLight);
		leftAxis.setValueFormatter(new LargeValueFormatter());
		leftAxis.setDrawGridLines(false);
		leftAxis.setSpaceTop(35f);
		leftAxis.setAxisMinimum(0f); // this replaces setStartAtZero(true)

		mChartView.getAxisRight().setEnabled(false);

		mChartView.getDescription().setEnabled(false);
		mChartView.getLegend().setHorizontalAlignment(Legend.LegendHorizontalAlignment.CENTER);

		mChartView.setFitBars(true);
	}

	private void UpdateChartValues() {
		int mNbCounters = mStatsData.GetNbCounters();
		Log.v(TAG, "Update " + mNbCounters + " values.");
		String[] labels = mStatsData.GetCounterNames();
		ArrayList[] values = new ArrayList[mNbCounters];

		double[] percents = mStatsData.GetDistribution();
		for (int k = 0; k < mNbCounters; k++)
		{
			values[k] = new ArrayList<BarEntry>();
			values[k].add(new BarEntry(0, (float) percents[k]));
			Log.v(TAG, "Update " + percents[k] + ".");
		}

		// Colors
		int LightGreen = Color.rgb(213, 249, 74);
		int LightOrange = Color.rgb(255, 204, 76);
		int LightBlue = Color.rgb(58, 196, 181);
		int[] barchartColors = new int[] { LightOrange, LightBlue, LightGreen};

		BarDataSet[] barDataSets = new BarDataSet[mNbCounters];

		for (int k = 0; k < mNbCounters; ++k) {
			barDataSets[k] = new BarDataSet(values[k], labels[k]);
			barDataSets[k].setColor(barchartColors[k]);
		}
		float barWidth = 0.45f; // x2 dataset
		BarData data = new BarData(barDataSets);
		data.setBarWidth(barWidth);

		mChartView.setData(data);
		float groupSpace = 0.06f;
		float barSpace = 0.02f;
		mChartView.groupBars(0.f, groupSpace, barSpace);


		mChartView.invalidate();
	}

	@Override
	protected void onPause() {
		super.onPause();  // Always call the superclass method first
		persistCurrentStatifier();
	}
	
	@Override
	protected void onStop() {
		super.onStop();
		persistCurrentStatifier();
	}

	private String getButtonText(int buttonIdx) {
		return mStatsData.getButtonText(buttonIdx);
	}

	private String getPercentText(int buttonIdx) {
		return mStatsData.getPercentText(buttonIdx);

	}

	private void showButtonsClicks() {
		TextView textView1 = findViewById(R.id.button1);
		textView1.setText(getButtonText(0));
		TextView textView2 = findViewById(R.id.button2);
		textView2.setText(getButtonText(1));

		TextView textViewPercent1 = findViewById(R.id.textViewPercent1);
		textViewPercent1.setText(getPercentText(0));
		TextView textViewPercent2 = findViewById(R.id.textViewPercent2);
		textViewPercent2.setText(getPercentText(1));		
	}

	private void vibrateOnClick(int buttonId) {
		mVibrator.vibrate(mVibrationPatterns[buttonId]);
	}

	/** Called when the user clicks the Send button */
	public void onClick(View view) {
		// Do something in response to button

		if (view.getId() == R.id.button1)
		{
			mStatsData.onClickCounter(0);
			vibrateOnClick(0);
		}
		else if(view.getId() == R.id.button2)
		{
			mStatsData.onClickCounter(1);
			vibrateOnClick(1);
		}
		mStatsData.updatePercents();
		showButtonsClicks();

		if (mChartView == null) {
			mChartView = findViewById(R.id.chart);
		}
		UpdateChartValues();
	}


	private void OnResetCount() {
		mStatsData.Reset();
		showButtonsClicks();
		if (mChartView == null) {
			mChartView = findViewById(R.id.chart);
		}
		UpdateChartValues();
	}


	/**
	 * Set up the {@link android.app.ActionBar}, if the API is available.
	 */
	@TargetApi(Build.VERSION_CODES.HONEYCOMB)
	private void setupActionBar() {
		if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.HONEYCOMB) {
			getActionBar().setDisplayHomeAsUpEnabled(true);
		}
	}

	public void showNoticeDialog() {
		// Create an instance of the dialog fragment and show it
		NoticeDialogFragment dialog = new NoticeDialogFragment();
		dialog.show(getSupportFragmentManager(), "NoticeDialogFragment");
	}

	@Override
	public void onDialogPositiveClick(
			DialogFragment dialog) {
		// User touched the dialog's positive button
		deleteCurrentStats();
		NavUtils.navigateUpFromSameTask(this);
	}

	@Override
	public void onDialogNegativeClick(
			DialogFragment dialog) {
		// User touched the dialog's negative button
		mIsStatDeleted = false;
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		switch (item.getItemId()) {
		case android.R.id.home:
			// This ID represents the Home or Up button. In the case of this
			// activity, the Up button is shown. Use NavUtils to allow users
			// to navigate up one level in the application structure. For
			// more details, see the Navigation pattern on Android Design:
			//
			// http://developer.android.com/design/patterns/navigation.html#up-vs-back
			//
			NavUtils.navigateUpFromSameTask(this);
			return true;
		case R.id.menu_delete:
			showNoticeDialog();
			return true;
		case R.id.menu_reset_count:
			// TODO: confirm with dialog
			OnResetCount();
		case R.id.menu_contextual_edit:
			//startEditActivity();
			return true;
		}
		return super.onOptionsItemSelected(item);
	}

}
