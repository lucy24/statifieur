package com.lucysoft.clickerz;

import java.util.ArrayList;
import java.util.List;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.database.sqlite.SQLiteQueryBuilder;
import android.util.Log;
import android.util.Pair;


public class ClickStorage extends SQLiteOpenHelper {
	private static final String TAG = "com.lucysoft.clickerz.ClickStorage";


	private static final int DATABASE_VERSION = 3;
	private static final String DATABASE_NAME = "stat_clicks.db";
	
	private SQLiteDatabase mDb = null;

	private static final String STAT_TABLE_CREATE =
			"CREATE TABLE " + StatsDatabaseContract.StatStruct.STATS_TABLE_NAME + " (" 
					+ StatsDatabaseContract.StatStruct._ID + " INTEGER PRIMARY KEY,"
					+ StatsDatabaseContract.StatStruct.STAT_COLUMN_NAME_TITLE + " TEXT,"
					+ StatsDatabaseContract.StatStruct.STAT_COLUMN_NAME_CREATE_DATE + " INTEGER,"
					+ StatsDatabaseContract.StatStruct.STAT_COLUMN_NAME_MODIFICATION_DATE + " INTEGER"
					+ ");";

	private static final String COUNTERS_TABLE_CREATE =
			"CREATE TABLE " + StatsDatabaseContract.CountersStruct.COUNTERS_TABLE_NAME + " (" 
					+ StatsDatabaseContract.CountersStruct._ID + " INTEGER PRIMARY KEY,"
					+ StatsDatabaseContract.CountersStruct.COUNTERS_COLUMN_NAME_TITLE + " TEXT,"
					+ StatsDatabaseContract.CountersStruct.COUNTERS_COLUMN_NAME_STAT_FK + " INTEGER"
					+ ");";

	private static final String CLICKS_TABLE_CREATE =
			"CREATE TABLE " + StatsDatabaseContract.ClickStruct.CLICKS_TABLE_NAME + " (" 
					+ StatsDatabaseContract.ClickStruct._ID + " INTEGER PRIMARY KEY,"
					+ StatsDatabaseContract.ClickStruct.CLICKS_COLUMN_NAME_COUNTER_FK + " INTEGER,"
					+ StatsDatabaseContract.ClickStruct.CLICKS_COLUMN_NAME_DATE + " INTEGER"
					+ ");";

	ClickStorage(Context context) {

		// calls the super constructor, requesting the default cursor factory.
		super(context, DATABASE_NAME, null, DATABASE_VERSION);
	}

	@Override
	public void onCreate(SQLiteDatabase db) {

		Log.v(TAG, "OK, create the tables...");
		db.execSQL(STAT_TABLE_CREATE);
		Log.v(TAG, "Stats [OK]");
		db.execSQL(COUNTERS_TABLE_CREATE);
		Log.v(TAG, "Counters [OK]");
		db.execSQL(CLICKS_TABLE_CREATE);
		Log.v(TAG, "Clicks [OK]");
	}

	@Override
	public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        Log.w(TAG, "Upgrading database from version " + oldVersion + " to "
                + newVersion + ", which will destroy all old data");
        // TODO fixme
        db.execSQL("DROP TABLE IF EXISTS " + StatsDatabaseContract.StatStruct.STATS_TABLE_NAME);
        db.execSQL("DROP TABLE IF EXISTS " + StatsDatabaseContract.CountersStruct.COUNTERS_TABLE_NAME);
        db.execSQL("DROP TABLE IF EXISTS " + StatsDatabaseContract.ClickStruct.CLICKS_TABLE_NAME);
        onCreate(db);
	}
	
	public void StartTransaction() {
		mDb = getWritableDatabase();
		mDb.beginTransaction();
	}
	
	public void Commit() {
		mDb.setTransactionSuccessful();
	}
	
	public void EndTransaction() {
		mDb.endTransaction();
	}
	
	public long insertValues(String table, ContentValues values) throws Exception {
		if(mDb == null)
			throw new Exception("database not opened");
		long insertId = -1;
		Log.v(TAG, "OK, add values...");
		Log.v(TAG, "transaction successful");
		insertId = mDb.insert(table, null, values);
		Log.v(TAG, "new row=" + insertId);
		return insertId;
	}
	
	public long updateValues(String table, ContentValues values, String whereClause, String[] whereArgs)  throws Exception {
		if(mDb == null)
			throw new Exception("database not opened");
		Log.v(TAG, "OK, change values...");
		Log.v(TAG, "transaction successful");
		int nbModified = mDb.update(table, values, whereClause, whereArgs);
		Log.v(TAG, "modified " + nbModified + " rows");
		return nbModified;
	}
	
	public long deleteValues(String table_name, String selection, String[] selectionArgs) throws Exception {
		if(mDb == null)
			throw new Exception("database not opened");
		Log.v(TAG, "OK, change values...");
		Log.v(TAG, "transaction successful");
		// Issue SQL statement.
		int nbDeleted = mDb.delete(table_name, selection, selectionArgs);
		Log.v(TAG, "deleted " + nbDeleted + " rows");
		return nbDeleted;
	}

	/**
	 * Performs a database query.
	 * @param selection The selection clause (ie the WHERE clause)
	 * @param selectionArgs Selection arguments for "?" components in the selection
	 * @param columns The columns to return (null for all)
	 * @return A Cursor over all rows matching the query
	 */
	public Cursor query(String selection, String[] selectionArgs, String[] columns, String tables, String orderBy) {
		/* The SQLiteBuilder provides a map for all possible columns requested to
		 * actual columns in the database, creating a simple column alias mechanism
		 * by which the ContentProvider does not need to know the real column names
		 */
		SQLiteQueryBuilder builder = new SQLiteQueryBuilder();
		builder.setTables(tables);

		Cursor cursor = builder.query(getReadableDatabase(),
				columns, selection, selectionArgs, null, null, orderBy);

		if (cursor == null) {
			return null;
		} else if (!cursor.moveToFirst()) {
			cursor.close();
			return null;
		}
		return cursor;
	}

	public Cursor getCursorOnStatTitles() {
		String[] columns = {StatsDatabaseContract.StatStruct._ID, StatsDatabaseContract.StatStruct.STAT_COLUMN_NAME_TITLE};
		Log.v(TAG, "run the query...");
		String orderBy = StatsDatabaseContract.StatStruct.STAT_DEFAULT_SORT_ORDER;
		Cursor cursor = query(null, null, columns, StatsDatabaseContract.StatStruct.STATS_TABLE_NAME, orderBy);
		Log.v(TAG, "[OK]");
		return cursor;
	}
	
	public List<Pair<String, Long>> get_stats() {
		Cursor cursor = getCursorOnStatTitles();
		List<Pair<String, Long>> stats_names = new ArrayList<Pair<String, Long>>();
		if (cursor == null) {
			Log.v(TAG, "but empty :-(");
			return stats_names;
		} else {
			Log.v(TAG, "yes! length=" + cursor.getCount());
			cursor.moveToFirst();
			
			do {
				Log.v(TAG, "cursor at" + cursor.getPosition());
				int sIndex = cursor.getColumnIndexOrThrow(StatsDatabaseContract.StatStruct._ID);
				int tIndex = cursor.getColumnIndexOrThrow(StatsDatabaseContract.StatStruct.STAT_COLUMN_NAME_TITLE);
				Log.v(TAG, "index=" + tIndex);
				String name = cursor.getString(tIndex);
				long id = cursor.getLong(sIndex);
				Log.v(TAG, "name=" + name);
				stats_names.add(Pair.create(name, id));
			} while(cursor.moveToNext());
			
		}
		return stats_names;
	}
}
